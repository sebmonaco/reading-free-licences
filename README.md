# Reading of free licences

## What's that !?
In this repository, I store the most common free licences in Markdown, I put them in shape (adding page breaks, defining titles level, list...), and I design the result with the help of TailwindCSS to add some typography basics. Then I do some HTML and PDF exports with the VSCode extension `Markdown PDF`... PDF are built to be printed but they can be read 'as is' as well.

## But why ?
First, I think it's important as free software developers and passionate users, to have read at least once all common free licences. Sometimes, we get a sense of what they mean, we read some parts and explanations, but we don't have a full understanding. I think that as we have to choose a specific licence for our programs, we have the responsability to fully understand what they include. We need to have at least a basic understanding of what is copyright, patents, trademarks, and public domain. Then, with this legal knowledge we can confidently copy code and sublicence some the code, notes, documentations and scripts we produce over the time.

*PS: not all common licences are present for the moment...*

## List of licences
### Permissive
- Apache License 2.0 (Apache-2.0)
- 3-clause BSD license (BSD-3-Clause)
- 2-clause BSD license (BSD-2-Clause)
- MIT license (MIT)
- The Unlicense (Unlicense)
- SIL Open Font License 1.1 (OFL-1.1)
- ISC License (ISC)

### Copyleft
- GNU General Public License (GPL)
- GNU Lesser General Public License (LGPL)
- GNU Affero General Public License version 3 (AGPL-3.0)
- Mozilla Public License 2.0 (MPL-2.0)

### Others
- Common Development and Distribution License 1.0 (CDDL-1.0)
- Eclipse Public License 2.0 (EPL-2.0)

### Creative commons suite
- CC BY 4.0
- CC BY-SA 4.0
- CC BY-NC 4.0
- CC BY-NC-ND 4.0
- CC0 1.0

### Non free (but used a bit)
- WTFPL v1 and v2
- Beerware license